//
//  ViewController_Table_Ext.swift
//  MatchStream
//
//  Created by Lakshmi Raj on 07/11/18.
//  Copyright © 2018 jeethukthomas. All rights reserved.
//

import UIKit

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.m_matches.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let matches = self.m_matches[section+1] {
           return matches.count
        }
        
       
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
       
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let header_height = tableView.frame.width / 10
        return header_height * 1.7
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "homeTableCellHeaderID") as! HomeTableCellHeader
        
         cell.title.text = "Match Details"
        
        if let matches = self.m_matches[section+1] {
            if matches.count > 0 {
            cell.title.text = matches[0].series_type
                 cell.img_league.downloadImageFrom(link: matches[0].mtype_img)
               
            }
            
            
        }
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "homeTableCellID") as! HomeTableCell
        
        cell.backgroundColor = UIColor.clear

        if let matches = self.m_matches[indexPath.section+1] {
            if matches.count > indexPath.row {
                let match = matches[indexPath.row]
                cell.lbl_team_a.text = match.team1_name
                cell.lbl_team_b.text = match.team2_name
               
                
                if let time = match.start_time {
                    cell.lbl_time.text = time
                }
                else {
                    cell.lbl_time.text = "00:00"
                }
                cell.imgTeam1.image = UIImage(named: "PlaceholderFootball")
                cell.imgTeam2.image = UIImage(named: "PlaceholderFootball")
                cell.imgTeam1.downloadImageFrom(link: match.team1_logo)
                cell.imgTeam2.downloadImageFrom(link: match.team2_logo)
            }
            //strArray[section]
        }
        
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let cell = tableView.dequeueReusableCell(withIdentifier: "homeTableCellID") as! HomeTableCell
        
       
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let matches = self.m_matches[indexPath.section+1] {
            if matches.count > indexPath.row {
                let match = matches[indexPath.row]
                self.performSegue(withIdentifier: "toDetails", sender: match)
            }
        }
//        var rows = [indexPath]
//        
//        if self.selectedIndex == indexPath {
//            self.selectedIndex = nil
//        }
//        else {
//            if self.selectedIndex != nil {
//                rows.append(self.selectedIndex!)
//            }
//            self.selectedIndex = indexPath
//        }
////        self.tableView.beginUpdates()
////
////        tableView.reloadRows(at: rows, with: UITableView.RowAnimation.fade)
//////        tableView.reloadSections(IndexSet(rows), with: UITableView.RowAnimation.fade)
////        self.tableView.endUpdates()
////        self.tableView.layer.removeAllAnimations()
////
////
////        self.tableView.reloadData()
//////        self.view.layoutIfNeeded()
//        
////        UIView.animate(withDuration: 2) {
//        
//            self.tableView.reloadData()
////        }
    }
}


func formatDate(fromDateString dateStr: String?) -> String?{
    if dateStr == nil {
        return nil
    }
    let calendar = Calendar.current
    guard let date = getDateFromString(fromDateString: dateStr!) else {
        return nil
    }
    let dateComponents = calendar.component(.day, from: date)
    let numberFormatter = NumberFormatter()
    
    numberFormatter.numberStyle = .ordinal
    
    let day = numberFormatter.string(from: dateComponents as NSNumber)
    let dateFormatter = DateFormatter()
    
    dateFormatter.dateFormat = "MMM"
    
    return "\(day!) \(dateFormatter.string(from: date))"
}

func getDateFromString(fromDateString dateStr: String) -> Date? {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "MM/dd/yyyy"
    guard let date = dateFormatter.date(from: dateStr) else {
        return nil
    }
    return date
}

func splitTeams(fromString str: String?) -> (String, String)? {
    if str == nil {
        return nil
    }
    
    let arr = str!.components(separatedBy: " vs ")
    
    if arr.count >= 2 {
        return (arr[0], arr[1])
    }
    else {
        let arr1 = str!.components(separatedBy: " ")
        if arr1.count == 2 {
            return (arr1[0], arr1[1])
        }
        else if arr1.count < 2 {
            return(str!,str!)
        }
        
    }
    
    return ("Team", str!)
}
