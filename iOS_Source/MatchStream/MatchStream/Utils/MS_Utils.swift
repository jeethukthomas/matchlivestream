//
//  MS_Utils.swift
//  MatchStream
//
//  Created by Lakshmi Raj on 07/11/18.
//  Copyright © 2018 jeethukthomas. All rights reserved.
//

import UIKit
import AVKit

class MS_Utils: NSObject {

    class func sortMatchBasedOnMatchType(theMatches matches: [Match]) -> [Int : [Match]] {
        
        var order = [Int : String]()
        
        var response = [String : [Match]]()
        
        for tMatch in matches {
            guard let _ = tMatch.series_type else {
                continue
            }
            if response.keys.contains(tMatch.series_type!) {
                response[tMatch.series_type!]?.append(tMatch)
            }
            else {
                order[order.count + 1] = tMatch.series_type!
                 response[tMatch.series_type!] = [tMatch]
            }
        }
        
        var originalData = [Int : [Match]]()
        for item in order {
            originalData[item.key] = response[item.value]
        }
        
        
        
        return originalData
       
        
    }
    
   
    class func startPlayingChannel() {
    
        guard let tChannel = channel_in_queue else{
            return
        }
        channel_in_queue = nil
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            if let nav = delegate.window?.rootViewController as? UINavigationController {
                
                if let urlStr = tChannel.http_stream {
                    if let tUrl = URL(string: urlStr) {
                        //                            let videoURL = URL(string: "http://stream.tvtap.live:8081/live/pt-sporttv3.stream/playlist.m3u8?wmsAuthSign=c2VydmVyX3RpbWU9MTEvMTIvMjAxOCAzOjAyOjE2IFBNJmhhc2hfdmFsdWU9L3Y2U3A3ZmF3cjFXWlRBdTN6ZlQvdz09JnZhbGlkbWludXRlcz0yMA==")
                        let player = AVPlayer(url: tUrl)
                        
                        let playerViewController = AVPlayerViewController()
                        playerViewController.player = player
                        
                        
                        nav.present(playerViewController, animated: true) {
                            player.play()
                        }
                    }
                }
                
                
                
            }
        }
        
    }
}


func rateApp(appId: String, completion: @escaping ((_ success: Bool)->())) {
    guard let url = URL(string : "itms-apps://itunes.apple.com/app/" + appId) else {
        completion(false)
        return
    }
    guard #available(iOS 10, *) else {
        completion(UIApplication.shared.openURL(url))
        return
    }
    UIApplication.shared.open(url, options: [:], completionHandler: completion)
}


extension UIImageView {
    func downloadImageFrom(link:String?) {
        guard let _ = link else {
            return
        }
        URLSession.shared.dataTask( with: NSURL(string:link!)! as URL, completionHandler: {
            (data, response, error) -> Void in
            DispatchQueue.main.async {
                if let data = data { self.image = UIImage(data: data) }
            }
        }).resume()
    }
}
