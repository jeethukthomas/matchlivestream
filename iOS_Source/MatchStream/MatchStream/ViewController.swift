//
//  ViewController.swift
//  MatchStream
//
//  Created by Lakshmi Raj on 04/11/18.
//  Copyright © 2018 jeethukthomas. All rights reserved.
//

import UIKit
import MessageUI

class ViewController: UIViewController, MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var seg3: UIButton!
    @IBOutlet weak var seg2: UIButton!
    @IBOutlet weak var seg1: UIButton!
    @IBOutlet weak var rightNavbtn: UIBarButtonItem!
    @IBOutlet weak var v_slideMenu: UIView!
    @IBOutlet weak var constraint_slideMenu: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var segSelectLeading: NSLayoutConstraint!
    
    var selectedDate = Date()
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.refreshData),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.darkGray
        refreshControl.attributedTitle = NSAttributedString(string: "Fetching...")
        return refreshControl
        
        
    }()
    
    var m_matches = [Int : [Match]]()
    var selectedIndex : IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SKActivityIndicator.show("Loading...")
        //        self.fetchAdWS()
//        self.refreshData()
        self.tableView.addSubview(self.refreshControl)
        
        
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width - 50, height: view.frame.height))
        titleLabel.text = "Home"
        titleLabel.textColor = UIColor.white
        navigationItem.titleView = titleLabel
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.layoutIfNeeded()
        self.toggleSlide(isHide: true)
        
        let btns = [seg1, seg2, seg3]
        for btn in btns {
            if btn?.isSelected ?? false {
                self.segClick(btn!)
            }
        }
    }
    
    
    
    
    func fetchAdWS() {
        let getSettings = GetAdsDetails()
        getSettings.start { (isSuccess, message) in
            
        }
        
        getSettings.getAdID { (isSuccess, message) in
            if isSuccess {
                DispatchQueue.main.async {
                    if let appDel = UIApplication.shared.delegate as? AppDelegate {
                        appDel.initializeAdMobBanner(isAtTop: false)
                        appDel.initializeInterstitial()
                    }
                }
                
                
            }
            
        }
    }
    
    @objc func refreshData() {
        let getMatch = GetAllLiveMatchDetails()
        getMatch.start(withDate: self.selectedDate) { (isSuccess, message) in
            DispatchQueue.main.async {
                SKActivityIndicator.dismiss()
                self.refreshControl.endRefreshing()
                if isSuccess {
                    if getMatch.matches.count == 0 {
                        let alert = UIAlertController(title: "No match found", message: "Please try again later", preferredStyle: UIAlertController.Style.alert)
                        
                        alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertAction.Style.destructive   , handler: { (action) in
                            
                        }))
                        self.present(alert, animated: true, completion: {
                            
                        })
                    }
                    self.m_matches = MS_Utils.sortMatchBasedOnMatchType(theMatches: getMatch.matches)
                    self.tableView.reloadData()
                }
                else {
                    let alert = UIAlertController(title: "Error while fetching the schedule", message: message, preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertAction.Style.default, handler: { (action) in
                        SKActivityIndicator.show("Loading...")
                        
                        self.refreshData()
                    }))
                    alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.destructive   , handler: { (action) in
                        
                    }))
                    self.present(alert, animated: true, completion: {
                        
                    })
                }
            }
            
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        
    }
    @IBAction func sideMenuToggle(_ sender: Any) {
        
        self.toggleSlide()
    }
    
    func toggleSlide(isHide toHide: Bool? = nil) {
        if let val = toHide {
            self.constraint_slideMenu.constant = val ? -self.v_slideMenu.frame.width : 0
        }
        else {
            if self.constraint_slideMenu.constant == 0{
                self.constraint_slideMenu.constant = -self.v_slideMenu.frame.width
            }
            else {
                self.constraint_slideMenu.constant = 0
            }
        }
        
        if self.constraint_slideMenu.constant == 0 {
            self.tableView.isUserInteractionEnabled = false
        }
        else {
            self.tableView.isUserInteractionEnabled = true
        }
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0.6, options: UIView.AnimationOptions.curveLinear, animations: {
            self.view.layoutIfNeeded()
        }) { (completed) in
            
        }
    }
    @IBAction func contactUsFn(_ sender: Any) {
        if !MFMailComposeViewController.canSendMail() {
            let alert = UIAlertController(title: "Device not supported!", message: "You cannot send emails from this device", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertAction.Style.destructive, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        
        // Configure the fields of the interface.
        composeVC.setToRecipients(["amileluke@gmail.com"])
        composeVC.setSubject("Feedback iOS")
        composeVC.setMessageBody(" ", isHTML: false)
        
        // Present the view controller modally.
        self.present(composeVC, animated: true, completion: nil)
        
    }
    @IBAction func rateFn(_ sender: Any) {
        
        rateApp(appId: k_appID) { (isSuccess) in
            if !isSuccess {
                let alert = UIAlertController(title: "Device not supported!", message: "Please try again later", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertAction.Style.destructive, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    @IBAction func shareFn(_ sender: Any) {
        let text = "The share content goes here. \n\n https://itunes.apple.com/us/app/id\(k_appID)"
        
        // set up activity view controller
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
        
        
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        // Check the result or perform other tasks.
        
        // Dismiss the mail compose view controller.
        controller.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func segClick(_ sender: UIButton) {
        let btns = [seg1, seg2, seg3]
        for btn in btns {
            if btn?.tag == sender.tag {
                sender.isSelected = true
                
                let offVal = (self.view.frame.width / 3 ) * CGFloat(sender.tag - 1)
                self.segSelectLeading.constant = CGFloat(offVal)
                UIView.animate(withDuration: 0.1, animations: {
                    self.view.layoutIfNeeded()
                })
                
                switch sender.tag {
                case 1:
                    let today = Date()
                    self.selectedDate = today.addingTimeInterval(-86400.0)
                case 2:
                    let today = Date()
                    self.selectedDate = today.addingTimeInterval(86400.0)
                default:
                    self.selectedDate = Date()
                }
            } else {
                sender.isSelected = false
            }
           
        }
        
        SKActivityIndicator.show("Loading...")
        self.refreshData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? MatchDetailsVC {
            if let tMatch = sender as? Match {
            vc.match = tMatch
            }
        }
    }
}

