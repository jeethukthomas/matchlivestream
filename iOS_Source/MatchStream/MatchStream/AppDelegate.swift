//
//  AppDelegate.swift
//  MatchStream
//
//  Created by Lakshmi Raj on 04/11/18.
//  Copyright © 2018 jeethukthomas. All rights reserved.
//

import GoogleMobileAds
import UIKit
import Fabric
import Crashlytics
import OneSignal

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GADBannerViewDelegate, GADInterstitialDelegate  {

    var window: UIWindow?
    var bannerView:GADBannerView!
    var interstitial: GADInterstitial!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        self.initializeAdMobBanner(isAtTop: false)
        self.initializeInterstitial()
              Fabric.with([Crashlytics.self])
        
        
//        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]
        
        // Replace 'YOUR_APP_ID' with your OneSignal App ID.
//        OneSignal.initWithLaunchOptions(launchOptions,
//                                        appId: "0de58eb1-6b5b-4758-8cf5-7ed2c935f873",
//                                        handleNotificationAction: nil,
//                                        settings: onesignalInitSettings)
        
        OneSignal.initWithLaunchOptions(launchOptions, appId: k_oneSignalID) { (result) in
            guard let result = result else {
                return
            }
            // Previously I used to use 'result.notification.wasAppInFocus == true'
            //            if result.notification.wasShown == false && result.notification.isSilentNotification == false {
            // App is in foreground
            // Showing my custom alert
            
            guard let payload = result.notification.payload else {
                return
            }
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1, execute: {
                let alert = UIAlertController(title: payload.title, message: payload.body, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertAction.Style.destructive, handler: nil))
                (self.window?.rootViewController as! UINavigationController).viewControllers.last?.present(alert, animated: true, completion: nil)
            })
            
            //            }
        }
        
        
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
        })
        
       
    
        
        return true
    }

    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


    func initializeAdMobBanner(isAtTop : Bool) {
        //        Init Banner View
        if self.bannerView == nil {
        self.bannerView = GADBannerView(adSize: (UIDevice.current.userInterfaceIdiom == .pad) ? kGADAdSizeFullBanner : kGADAdSizeSmartBannerPortrait)
            self.bannerView?.delegate = self
        }
        self.bannerView?.frame.origin = CGPoint(x: (self.window!.frame.width-self.bannerView!.frame.width)/2, y: isAtTop ? 0 : (self.window!.frame.height-self.bannerView!.frame.height))
        self.bannerView?.adUnitID = k_admob_banner_id
        self.bannerView?.rootViewController = self.window?.rootViewController
        //        tSelf.view.insertSubview(self.bannerView!, at: 999)
        self.window?.addSubview(self.bannerView!)
        let gadR = GADRequest()
        gadR.testDevices = ["60f27aaa5eab79918846bedbd2967ea8", kGADSimulatorID];
        self.bannerView?.load(gadR)
    }
    
  
    
    
    func showInterstetialAd() {
         if let nav = self.window?.rootViewController as? UINavigationController {
             self.interstitial?.present(fromRootViewController: nav.viewControllers.last!)
        }
        
    }
    
    func initializeInterstitial() {
        let appDelegate = self
        if self.interstitial == nil {
        appDelegate.interstitial = GADInterstitial(adUnitID: k_admob_interstetial_id)
        appDelegate.interstitial?.delegate = appDelegate
        }
        let gadR = GADRequest()
        gadR.testDevices = ["60f27aaa5eab79918846bedbd2967ea8", kGADSimulatorID];
        appDelegate.interstitial?.load(gadR)
        
        
    }
    
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
//        if let rootVC = UIApplication.shared.keyWindow?.rootViewController{
////            self.interstitial?.present(fromRootViewController: rootVC)
//        }
    }
    
    
    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
        self.initializeInterstitial()
    }
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        if k_ad_befor_play {
            MS_Utils.startPlayingChannel()
        }
        
        let gadR = GADRequest()
        gadR.testDevices = ["60f27aaa5eab79918846bedbd2967ea8", kGADSimulatorID];
        self.interstitial?.load(gadR)
    }
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("BANNER RECEIVED")
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        self.initializeAdMobBanner(isAtTop: false)
    }
}

