//
//  HomeTableCell.swift
//  MatchStream
//
//  Created by Lakshmi Raj on 07/11/18.
//  Copyright © 2018 jeethukthomas. All rights reserved.
//

import UIKit

protocol HomeTableCellDelegate {
    func selected()
}
class HomeTableCell: UITableViewCell {

    @IBOutlet weak var lbl_team_b: UILabel!
    @IBOutlet weak var lbl_time: UILabel!
    @IBOutlet weak var lbl_team_a: UILabel!
    @IBOutlet weak var cell_card: UIView!
    @IBOutlet weak var imgTeam1: UIImageView!
    @IBOutlet weak var imgTeam2: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.cell_card.layer.shadowOpacity = 0.15
        self.cell_card.layer.shadowOffset = CGSize(width: 1, height: 0)
        self.cell_card.layer.shadowRadius = 7
        self.cell_card.layer.shadowColor = UIColor.black.cgColor
        self.cell_card.layer.masksToBounds = false
        self.cell_card.layer.cornerRadius = 7;
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
}


class HomeTableCellHeader: UITableViewCell {
    
    @IBOutlet weak var img_league: UIImageView!
    @IBOutlet weak var title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

class HomeTableCellChannelCell: UICollectionViewCell {
    
    @IBOutlet weak var lbl_channel_name: UILabel!
    @IBOutlet weak var img_channel: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
    }
    
    
    
}
