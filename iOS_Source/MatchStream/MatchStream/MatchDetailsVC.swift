//
//  MatchDetailsVC.swift
//  MatchStream
//
//  Created by Jeethu Thomas on 30/08/19.
//  Copyright © 2019 jeethukthomas. All rights reserved.
//

import UIKit

class MatchDetailsVC: UIViewController {

    var match: Match!
    
    @IBOutlet weak var lblCoverTeam2: UILabel!
    @IBOutlet weak var lblCoverTeam1: UILabel!
    @IBOutlet weak var imgCoverTeam2: UIImageView!
    @IBOutlet weak var imgCoverTeam1: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.refreshUX()
    }
    func refreshUX() {
        self.lblCoverTeam1.text = self.match.team1_name
        self.imgCoverTeam1.downloadImageFrom(link: self.match.team1_logo)
        
        self.lblCoverTeam2.text = self.match.team2_name
        self.imgCoverTeam2.downloadImageFrom(link: self.match.team2_logo)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
