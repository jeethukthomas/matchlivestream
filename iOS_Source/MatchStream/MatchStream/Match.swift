//
//  Match.swift
//  MatchStream
//
//  Created by Lakshmi Raj on 06/11/18.
//  Copyright © 2018 jeethukthomas. All rights reserved.
//

import UIKit

@objcMembers
class Match: NSObject {
    var pk_id: String!
    var team1_name: String?
    var series_type: String?
    var id_matches_leaguepass: String?
    var venue: String?
    var start_time: String?
    var end_time: String?
    var start_date: String?
    var match_type_id: String?
    var is_live: String?
    
    var created_ts: String?
    var date: String?
    var time: String?
    var game: String?
    var status: String?
    var team1_result: String?
    var team2_result: String?
    var team1_logo: String?
    var team2_logo: String?
    var thumbnail: String?
    var thumbnailv2: String?
    var result: String?
    var team2_name: String?
    var name: String?
    var mtype_img: String?
    
    var edited_Date: Date?
    
    var channels = [Channel]()
    
    init(jsonObject data: [String : Any]) {
        super.init()
        for key in data.keys {
            
            if key == "channels" {
                if let channelJson = data[key] as? [[String : Any]] {
                    self.channels.removeAll()
                    for item in channelJson {
                        self.channels.append(Channel(jsonObject: item))
                    }
                }
            }
            else if self.responds(to: NSSelectorFromString(key)) {
                self.setValue(data[key], forKey: key)
                
                if key == "start_date" {
                    if let tDate = self.start_date {
                    self.edited_Date = getDateFromString(fromDateString: tDate)
                    }
                }
            }
        }
    }
}
