//
//  WebServiceConnector.swift
//
//  Created by Jeethu Thomas on 1/27/16.
//  Copyright © 2016. All rights reserved.
//

import UIKit

typealias CompletionClosure = (_ success : Bool, _ message : String?) -> Void

class WebServiceConnector: NSObject , URLSessionDelegate, URLSessionDataDelegate {
    
    func connect(urlRequest: URLRequest, _ completionClosure: @escaping CompletionClosure) {
        if Reachability()!.isReachable {
            
                performURLOperation(withRequest: urlRequest,  completionClosure)
            
        }
        else {
            DispatchQueue.main.async{
                completionClosure(false, "No Internet Connectivity");
            }
        }
    }
    
    func uploadFile(urlRequest: URLRequest,andFile file: URL, _ completionClosure: @escaping CompletionClosure) {
        if Reachability()!.isReachable {
            
            performFileUpload(withRequest: urlRequest, andFile: file,  completionClosure)
            
        }
        else {
            DispatchQueue.main.async{
                completionClosure(false, "No Internet Connectivity");
            }
        }
    }
    
    func performURLOperation(withRequest tRequest: URLRequest, _ completion: @escaping CompletionClosure) {

        let tsession = URLSession(configuration: URLSessionConfiguration.default, delegate: self, delegateQueue: nil)
    
        let task = tsession.dataTask(with: tRequest) { (data, response, error) in
            if let _ = data, error == nil {
                let finalValue = self.parseData(data!)
                
                print("################################ RESULT = \(finalValue.1)  #######################")
                completion(finalValue.0, finalValue.1)
            }
            else {
                DispatchQueue.main.async{
                    completion(false, error!.localizedDescription);
                }
            }
        }
        
        
        
        
       
        task.resume()
    }
    
    func performFileUpload(withRequest tRequest: URLRequest,andFile file: URL, _ completion: @escaping CompletionClosure) {
        
        let tsession = URLSession(configuration: URLSessionConfiguration.default, delegate: self, delegateQueue: nil)
        
        let task = tsession.uploadTask(with: tRequest, fromFile: file) { (data, response, error) in
            if let _ = data, error == nil {
                let finalValue = self.parseData(data!)
                
                print("################################ RESULT = \(finalValue.1)  #######################")
                completion(finalValue.0, finalValue.1)
            }
            else {
                DispatchQueue.main.async{
                    completion(false, error!.localizedDescription);
                }
            }
        }
        
        
        
        
        
        task.resume()
    }
    
    func checkCustomResponseCode(withCode tCode: String) -> (Bool, String) {
        if let codeVal = Int(tCode) {
            switch codeVal{
            case 20:
                return (true, "SUCCESS")
            case 40:
                return (false, "Error")
            case 41:
                return (false, "Unauthorized")
            case 201:
                return (true, "Created")
            case 401:
                return (false, "Unauthorized")
            case 403:
                return (false, "Forbidden")
            case 404:
                return (false, "Not Found")
                
            default:
                return (false, "Error")
            }
        }
        else {
            return (false, "Invalid response code")
        }
    }
    func parseData(_ data : Data) -> (Bool, String) {
//        do {
//            let jsonString = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments)
//
//
//            if let dictFromJSON = jsonString as? [String:Any] {
//                if let dictData = dictFromJSON["status"] as? [String:Any] {
//                    if let tCode = dictData["responseCode"] as? String {
//
//                        var res = self.checkCustomResponseCode(withCode: tCode)
//
//                          if let resDictData = dictFromJSON["result"] as? [String:Any] {
//                             if let tMsg = resDictData["message"] as? String {
//                                res.1 = tMsg
//                            }
//                        }
//
//                        return res
//
//                    }
//                }
//
//            }
//
//
//
//
//        }
//        catch {
//            return (false, "Invalid response")
//        }
        
        
        return (true, "Super Success")
    }
    
}


