//
//  UPT_RequestType.swift
//  Up Part Time
//
//  Created by Jeethu Thomas on 24/09/18.
//  Copyright © 2018 UpPartTime. All rights reserved.
//

import UIKit
import MobileCoreServices

enum UPT_URLType: String {
    case GET = "GET"
    case POST = "POST"
}

class UPT_RequestType {
    
    func createRequestWith(urlEndpoint url: URL, urlType type:UPT_URLType = .GET, requestBody body: [String : Any]? = nil, isHeadersNeeded: Bool = false) -> URLRequest{
    var _request = URLRequest(url: url)
    _request.httpMethod = type.rawValue
        
        
        if isHeadersNeeded {
            _request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
            _request.setValue("37a6259cc0c1dae299a7866489dff0bd", forHTTPHeaderField: "app-token")

        }

        
        if let tBody = body {
            do {
    _request.httpBody = try JSONSerialization.data(withJSONObject: tBody, options: JSONSerialization.WritingOptions.prettyPrinted)
            }
            catch {
                print("Invalid body format")
            }
        }
    return _request
    }
    
    func createMultipartRequestWith(urlEndpoint url: URL, urlType type:UPT_URLType = .GET, requestBody body: [String : Any]? = nil) -> URLRequest{
        var _request = URLRequest(url: url)
        _request.httpMethod = type.rawValue
        let boundary = "Boundary-\(UUID().uuidString)"
        _request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        _request.setValue("gmt", forHTTPHeaderField: "Locale")
        
        
        if let tBody = body {
            do {
                _request.httpBody = try JSONSerialization.data(withJSONObject: tBody, options: JSONSerialization.WritingOptions.prettyPrinted)
            }
            catch {
                print("Invalid body format")
            }
        }
        return _request
    }
    
    // For testing
    
    func createMultipartRequestWith(urlEndpoint url: URL, urlType type:UPT_URLType = .GET, requestBody body: [String : Any]? = nil, andWithFile file: URL) -> URLRequest{
        var _request = URLRequest(url: url)
        _request.httpMethod = type.rawValue
        let boundary = "Boundary-\(UUID().uuidString)"
        _request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        _request.setValue("gmt", forHTTPHeaderField: "Locale")
        
        
        do {
            _request.httpBody = try createBody(with: nil, filePathKey: "file", paths: [file], boundary: boundary)
        }
        catch {
            print("Invalid body format")
        }
        return _request
    }
    
    /// Create body of the `multipart/form-data` request
    ///
    /// - parameter parameters:   The optional dictionary containing keys and values to be passed to web service
    /// - parameter filePathKey:  The optional field name to be used when uploading files. If you supply paths, you must supply filePathKey, too.
    /// - parameter paths:        The optional array of file paths of the files to be uploaded
    /// - parameter boundary:     The `multipart/form-data` boundary
    ///
    /// - returns:                The `Data` of the body of the request
    
    private func createBody(with parameters: [String: String]?, filePathKey: String, paths: [URL], boundary: String) throws -> Data {
        var body = Data()
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.append("--\(boundary)\r\n")
                body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.append("\(value)\r\n")
            }
        }
        
        for path in paths {
            let url = path
            let filename = url.lastPathComponent
            let data = try Data(contentsOf: url)
            let mimetype = mimeType(for: path)
            
            body.append("--\(boundary)\r\n")
            body.append("Content-Disposition: form-data; name=\"\(filePathKey)\"; filename=\"\(filename)\"\r\n")
            body.append("Content-Type: \(mimetype)\r\n\r\n")
            body.append(data)
            body.append("\r\n")
        }
        
        body.append("--\(boundary)--\r\n")
        return body
    }
    
    /// Determine mime type on the basis of extension of a file.
    ///
    /// This requires `import MobileCoreServices`.
    ///
    /// - parameter path:         The path of the file for which we are going to determine the mime type.
    ///
    /// - returns:                Returns the mime type if successful. Returns `application/octet-stream` if unable to determine mime type.
    
    
    private func mimeType(for path: URL) -> String {
        let url = path
        let pathExtension = url.pathExtension
        
        if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension as NSString, nil)?.takeRetainedValue() {
            if let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
                return mimetype as String
            }
        }
        return "application/octet-stream"
    }

}



extension Data {
    
    /// Append string to Data
    ///
    /// Rather than littering my code with calls to `data(using: .utf8)` to convert `String` values to `Data`, this wraps it in a nice convenient little extension to Data. This defaults to converting using UTF-8.
    ///
    /// - parameter string:       The string to be added to the `Data`.
    
    mutating func append(_ string: String, using encoding: String.Encoding = .utf8) {
        if let data = string.data(using: encoding) {
            append(data)
        }
    }
}

