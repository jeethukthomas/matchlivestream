//
//  GetAdsDetails.swift
//  MatchStream
//
//  Created by Lakshmi Raj on 13/11/18.
//  Copyright © 2018 jeethukthomas. All rights reserved.
//

import UIKit

class GetAdsDetails:  WebServiceConnector {
    
    
    func start(_ completionClosure: @escaping CompletionClosure) {
        
        
        
        let path = k_BaseUrl + API_Endpoint.get_ads_settings.rawValue
        if let tUrl = URL(string: path) {
            
            let urlRequest = UPT_RequestType().createRequestWith(urlEndpoint: tUrl, urlType: UPT_URLType.GET,requestBody: nil, isHeadersNeeded: true)
            super.connect(urlRequest: urlRequest) { (isSuccess, message) in
                completionClosure(isSuccess, message)
            }
        }
        else {
            completionClosure(false, "Invalid Input Data")
        }
        
    }
    
    func getAdID(_ completionClosure: @escaping CompletionClosure) {
        
        
        
        let path = k_BaseUrl + API_Endpoint.get_ads_ids.rawValue
        if let tUrl = URL(string: path) {
            
            let urlRequest = UPT_RequestType().createRequestWith(urlEndpoint: tUrl, urlType: UPT_URLType.GET,requestBody: nil, isHeadersNeeded: true)
            super.connect(urlRequest: urlRequest) { (isSuccess, message) in
                completionClosure(isSuccess, message)
            }
        }
        else {
            completionClosure(false, "Invalid Input Data")
        }
        
    }
    
    override func parseData(_ data: Data) -> (Bool, String) {
        
        
        do {
            let jsonString = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments)
            
            
            if let mainJson = jsonString as? [String:Any] {
                
                if let suc = mainJson["success"] as? Int{
                    if suc == 1 {
                        if let dictFromJSON = mainJson["msg"] as? [String:Any] {
                            
                            if let lastJson = dictFromJSON["ad_options"] as? [String:Any] {
                                
                                if let val = lastJson["playerad_position"] as? String{
                                    if val == "0" {
                                        k_ad_befor_play = false
                                        
                                    return (true,  "Success")
                                    }
                                    else if val == "1"{
                                        k_ad_befor_play = true
                                        
                                        return (true,  "Success")
                                    }
                                }
                            }
                            else if let lastJson = dictFromJSON["ads"] as? [[String:Any]] {
                                
                                var status = 0
                                for item in lastJson {
                                    
                                    if let val = item["pk_id"] as? String{
                                        if val == "3" {
                                            
                                            if let adID = item["code"] as? String{
                                                k_admob_banner_id = adID
                                                status += 1
                                            }
                                           
                                        }
                                        else if val == "4" {
                                            
                                            if let adID = item["code"] as? String{
                                                k_admob_interstetial_id = adID
                                                status += 1
                                            }
                                            
                                        }
                                    }
                                    
                                }
                                if status >= 1 {
                                    return (true,  "Success")
                                }
                            }
                        }
                    }
                }
            }
            
            
            
            
            
            
            
            
        }
        catch {
            return (false, "Invalid response")
        }
        
        return (false, "Error in received data")
    }
    
    
    
}
