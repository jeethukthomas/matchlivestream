//
//  GetAllMatchDetails.swift
//  MatchStream
//
//  Created by Lakshmi Raj on 06/11/18.
//  Copyright © 2018 jeethukthomas. All rights reserved.
//

import UIKit

class GetAllLiveMatchDetails: WebServiceConnector {

    var matches = [Match]()
    
    
    func start(withDate date: Date = Date(), _ completionClosure: @escaping CompletionClosure) {
        
        
        let path = "http://api.usatvhub.com/index_tv112.php?case=get_live_matches_by_date&date=" + String(date.timeIntervalSince1970)
//        let path = k_BaseUrl + API_Endpoint.get_all_live_match.rawValue
        if let tUrl = URL(string: path) {
            
            var tBody = [String:String]()
            tBody["username"] = "-1"
            
            let urlRequest = UPT_RequestType().createRequestWith(urlEndpoint: tUrl, urlType: UPT_URLType.GET,requestBody: nil, isHeadersNeeded: false)
            super.connect(urlRequest: urlRequest) { (isSuccess, message) in
                completionClosure(isSuccess, message)
            }
        }
        else {
            completionClosure(false, "Invalid Input Data")
        }
        
    }

    
    
    override func parseData(_ data: Data) -> (Bool, String) {
        
        
        do {
            let jsonString = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments)
            
            
            if let mainJson = jsonString as? [String:Any] {
                
                if let suc = mainJson["success"] as? Int{
                    if suc == 1 {
                        if let dictFromJSON = mainJson["msg"] as? [String:Any] {
                            
                            if let matchesJson = dictFromJSON["matches"] as? [[String : Any]] {
                            
                            
                            self.matches.removeAll()
                            for item in matchesJson {
                                self.matches.append(Match(jsonObject: item))
                            }
                                
                                self.matches = self.matches.sorted{$1.edited_Date! > $0.edited_Date!}
//                                self.matches = self.matches.sorted{$1.start_time! > $0.start_time!}
                                
                            return (true, "Success")
                            }
                        }
                    }
                }
            }
            
            
            
            
            
            
            
            
        }
        catch {
            return (false, "Invalid response")
        }
        
        return (false, "Error in received data")
    }

    
    
}
