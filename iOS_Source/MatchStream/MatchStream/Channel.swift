//
//  Channel.swift
//  MatchStream
//
//  Created by Lakshmi Raj on 06/11/18.
//  Copyright © 2018 jeethukthomas. All rights reserved.
//

import UIKit
@objcMembers
class Channel: NSObject {

    var pk_id: String!
    var channel_name: String?
    var img: String?
    var link_to_play: String?
    var http_stream: String?
    var http_stream2: String?
    var http_stream3: String?
    var rtmp_stream: String?
    var rtmp_stream2: String?
    var rtmp_stream3: String?
    var country: String?
    var epg_link: String?
    var epg_link_usa: String?
    var cat_id: String?
    var cat_name: String?
    var chrome_cast_link1: String?
     var chrome_cast_link2: String?
     var chrome_cast_link3: String?
    
    init(jsonObject data: [String : Any]) {
        super.init()
        for key in data.keys {
            if self.responds(to: NSSelectorFromString(key)) {
                self.setValue(data[key], forKey: key)
            }
        }
    }
    
    
}
