//
//  NotificationViewController.swift
//  MatchStreamNotiService
//
//  Created by Lakshmi Raj on 22/11/18.
//  Copyright © 2018 jeethukthomas. All rights reserved.
//

import UIKit
import UserNotifications
import UserNotificationsUI

import OneSignal

class NotificationViewController: UIViewController, UNNotificationContentExtension {

    @IBOutlet var label: UILabel?
    
    var contentHandler: ((UNNotificationContent) -> Void)?
    var receivedRequest: UNNotificationRequest!
    var bestAttemptContent: UNMutableNotificationContent?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any required interface initialization here.
    }
    
   
    func didReceive(_ notification: UNNotification) {
        self.label?.text = notification.request.content.body
        
        self.receivedRequest =  notification.request;
        bestAttemptContent = (notification.request.content.mutableCopy() as? UNMutableNotificationContent)
        
        if let _ = bestAttemptContent {
            OneSignal.didReceiveNotificationExtensionRequest(self.receivedRequest, with: self.bestAttemptContent)
        }
        
        
    }

   
    
    
}
